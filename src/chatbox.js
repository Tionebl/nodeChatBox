const mqtt = require('mqtt');
const readline = require('readline');

// Connectez-vous au broker MQTT
const client = mqtt.connect('mqtt://test.mosquitto.org:1883');

// Demandez à l'utilisateur de saisir son nom d'utilisateur
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let username = '';

rl.question('Entrez votre nom d\'utilisateur : ', (name) => {
  username = name;

  client.subscribe('chat');
  client.subscribe(`chat/${username}`);

  client.publish('chat', `${username} a rejoint le chat.`);

  client.on('message', (topic, message) => {
    console.log(`${message}`);
  });

  rl.on('line', (input) => {
    if (input.startsWith('/w')) {
      const parts = input.split(' ');
      const targetUser = parts[1]
      const message = parts.slice(2).join(' ');
      client.publish(`chat/${targetUser}`, `[whisp] ${username}: ${message}`);
    } else {
      // envoyez le message à tous les utilisateurs
      client.publish('chat', `${username}: ${input}`);
    }
  });
});