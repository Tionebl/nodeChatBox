const mqtt = require('mqtt');
const readline = require('readline');


const client = mqtt.connect('mqtt://test.mosquitto.org:1883');


// Demandez à l'utilisateur de saisir son nom d'utilisateur
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  
  
  rl.question('Entrez votre nom d\'utilisateur : ', (username) => {
    // Abonnez-vous au canal de chat
    client.subscribe('chat');
  
    // Envoyez un message à tous les utilisateurs pour signaler votre arrivée
    client.publish('chat', `${username} a rejoint le chat.`);
  
    // Lorsque vous recevez un message, affichez-le dans la console
    client.on('message', (topic, message) => {
      console.log(`${message}`);
    });
  
    // Lorsque l'utilisateur saisit un message, envoyez-le à tous les utilisateurs
    rl.on('line', (input) => {
      client.publish('chat', `${username}: ${input}`);
    });
  });